$.material.init();

var json = {
    title: "Are You Productive? ",
    showProgressBar: "top",
    pages: [{
        questions: [{
            type: "matrix",
            name: "Read each statement and circle the number that best reflects the frequency with which you demonstrate each behavior. 1 is Seldom 2 is Sometimes 3 is Frequently",
            
            title: "Read each statement and circle the number that best reflects the frequency with which you demonstrate each behavior. 1 is Seldom 2 is Sometimes 3 is Frequently",
            columns: [{
                value: 1,
                text: "Always"
            }, {
                value: 2,
                text: "Usually"
            }, {
                value: 3,
                text: "Often"
            }],
            rows: [{
                value: "spend",
                text: "I spend 10–15 minutes planning my day."
            }, {
                value: "check",
                text: "I check every email as soon as it arrives."
            }, {
                value: "day",
                text: "My day is organized by the key results I need to achieve."
            }, {
                value: "files",
                text: "My desk and files are a mess."
            }, {
                value: "pockets",
                text: "I leave open pockets of time in my schedule."
            }, {
                value: "late",
                text: "I’m habitually late."
            },{
                value: "focus",
                text: "I focus on one activity at a time."
            },{
                value: "late",
                text: "I’m habitually late."
            },{
                value: "done",
                text: "I multi-task to try to get more done"
            },{
                value: "breaks",
                text: "I take mini-breaks throughout the day."
            },{
                value: "seek",
                text: "I actively seek out projects that stretch my skills."
            },{
                value: "rut",
                text: "I feel I am in a rut at work."
            },]
        }]
        }]
   }     
Survey.defaultBootstrapMaterialCss.navigationButton = "btn btn-green";
Survey.defaultBootstrapMaterialCss.rating.item = "btn btn-default my-rating";
Survey.Survey.cssType = "bootstrapmaterial";

var survey = new Survey.Model(json);

survey.onComplete.add(function(result) {

	var sum = 0;
	var rows = Object.values(result.data);
	for(var i = 0; i < rows.length; i++){
		var row = Object.values( rows[i] );
		for(var j = 0; j < row.length; j++) {
			var val = row[j];
			sum += parseInt(val);
		}
	}


function myFunction(sum) 

{
    var scoring;
    if (sum <= 35 && sum >= 31) {
        scoring = "Effective Listener";
    } else if (sum <= 30 && sum >= 21) {
        scoring = "Good Listner";
    } else if (sum <= 20 && sum >= 14) {
        scoring = "Not-So-Good Listener";
    } else if (sum <= 13 && sum >=0) {
        scoring = "Huh?";
    } else {
        scoring = "Error";
    }
    
document.querySelector('#result').innerHTML = "Your Score " + sum + "<br>" + "You Are a(n) " + scoring;

} 

myFunction(sum);

});
       
survey.render("surveyElement");